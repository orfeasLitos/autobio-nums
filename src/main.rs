// Naive implementation, only for base 10

use std::convert::TryInto;

const BASE: u128 = 10;

fn autobio(length: usize) -> Vec<u128> {
    let mut res = vec![];
    'outer: for i in 0..((BASE.pow(length.try_into().unwrap()) - 1) as u128) {
        if i % BASE != 0 { // autobiographical numbers are multiples of the base
            continue;
        }
        let digits = format!("{:0>1$}", i.to_string(), length);

        if digits // if there is any digit equal or larger than the length, the number is invalid
            .chars()
            .filter(|x| {
                    x.to_digit(BASE.try_into().unwrap()).unwrap() >= length.try_into().unwrap()
                })
            .next()
            .is_some() {
            continue;
        }

        for j in 0..length {
            let count = digits
                .chars()
                .filter(|x| {
                    x.to_digit(BASE.try_into().unwrap()).unwrap() == j.try_into().unwrap()
                })
                .count();
            if count as u32 != digits.chars().nth(j).unwrap().to_digit(BASE.try_into().unwrap()).unwrap() {
                continue 'outer;
            }
        }
        res.push(i);
    }
    res
}

fn main() {
    for length in 1..=10 {
        println!("Length {}: {:?}", length, autobio(length));
    }
}
